#!/bin/bash
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

/usr/libexec/wassh-decorate --help | grep Usage && echo_info "Usage: OK" || false

# pass through exit code
/usr/libexec/wassh-decorate --label TRUE true && echo_info "exit code 0: OK" || false
/usr/libexec/wassh-decorate --label FALSE false && false || echo_info "exit code 1: OK" || false

# suppress label on empty output
/usr/libexec/wassh-decorate --label ZERO --nonull true | grep ZERO || echo_info "nonull: OK" || false

# decorate STDERR
/usr/libexec/wassh-decorate --label DECO ls /nosuchfile_deco |& cat -A | grep -q '032;01' && echo_info 'decoration: OK' || false

# fail for timeout
/usr/libexec/wassh-decorate --label TIMEOUT_TEST --timeout 1 'sleep 5' && false || echo_info "timeout: OK" || false

echo_info "wassh-decorate: all passed"
