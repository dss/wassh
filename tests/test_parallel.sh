#!/bin/bash
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# basic syntax test
/usr/libexec/parallel --help | grep Usage && echo_info 'Usage: OK' || false

# check that output per-subprocess stays together, even if slow
echo -e "echo 1 FOO; sleep 5; echo 2 BAR\necho 3 BAZ\nsleep 10; echo 4 GONZO\necho 5 QUX" | /usr/libexec/parallel | sort --check && echo_info 'output order: OK' || false

# check that slow output gets suppressed
echo -e "echo 1 FOO; sleep 2; echo 2 BAR\necho 3 BAZ\nsleep 10; echo 1 WRONG" | /usr/libexec/parallel --timeout 4 | sort --check && echo_info 'timeout output: OK' || false

# check that slow output causes errors
echo -e "echo 1 FOO; sleep 2; echo 2 BAR\necho 3 BAZ\nsleep 10; echo 1 WRONG" | /usr/libexec/parallel --timeout 4 && false || echo_info 'timeout status: OK' || false

# check that random gunk goes through OK
count=$(echo -e "dd if=/dev/urandom bs=512 count=1 status=none\necho 123456789" | /usr/libexec/parallel | wc -c)
if [[ $count = $((512+10)) ]]; then
  echo_info 'random bytes: OK'
else
  echo_error "FAIL random byte count=$count"
  false
fi

# lots of decorations, but no STDOUT
echo true | /usr/libexec/parallel --debug | wc -c | grep '^0$' && echo_info '--debug: OK' || false

echo_info "parallel: all tests OK"
