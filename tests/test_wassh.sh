#!/bin/bash
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e
wassh --help | grep Usage && echo_info "Usage: OK" || false

# pure DNS lookups
wassh --list aiadm[00-99] && echo_info "DNS list: OK" || false
wassh --list no-suchbox[0-9] && false || echo_info "DNS not found: OK" || false

# SSM tests would need credentials
rpm -ql wassh-ssm-cern | grep 'wassh_ssm/.*\.pm' && echo_info "SSM modules: OK" || false

# actual SSH logins would need credentials
if [[ -n "${KOJICI_USER}" ]]; then
  echo_command "Doing authenticated tests"
  echo "${KOJICI_PWD}" | kinit "${KOJICI_USER}@CERN.CH" && echo_info "got Kerberos credential for ${KOJICI_USER}" || false
  wassh --verbose --debug 7 --ssm puppetdb --list --cl aiadm/nodes/login && echo_info "PuppetDB list: OK" || false
  # SSM:Foreman - only works for admin accounts - skip
  # SSH: unclear whether CI accounts can login on LXPLUS, skip
else
  echo_error "WARN: skipped authenticated tests, no KOJICI_USER"
fi

echo_info "wassh: all tests passed"
