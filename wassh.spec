Summary: pluggable parallel SSH Client
Name: wassh
Version: 3.6.0
Vendor: CERN
Release: 1%{?dist}
# was http://cern.ch/eu-datagrid/license.html
License: EU Datagrid
Group: Applications/Internet
URL: https://gitlab.cern.ch/dss/wassh
Source: wassh-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-build
#Packager: Rohitashva Sharma <Rohitashva.Sharma@barc.gov.in>

# per perl-CAF, perl-LC
Requires: perl-AppConfig
Requires: perl-Proc-ProcessTable
Requires: perl-IO-String
Requires: perl-Sys-Syslog
# separate RPMs on CS9 so use virtual provides
Requires: perl(diagnostics)
Requires: perl(English)
# we should have no other remaining user for these modules
Obsoletes: perl-CAF <= 1.9.0
Obsoletes: perl-LC <= 1.1.2

# needed for man pages..
BuildRequires: /usr/bin/docbook2man
# needed for ssh-probe
BuildRequires: gcc
# needed for perl-related RPM macros
BuildRequires: perl-macros

%description
Wide area SSH client
 https://gitlab.cern.ch/dss/wassh

%package ssm-cern
Summary: Site Specific Modules for CERN
Group: Applications/Internet
Requires: wassh
Requires: perl-JSON
BuildArch: noarch
%description ssm-cern
This package contains the site specific module for CERN to be used with Wassh.
This provides an interface to query PuppetDB and Foreman to generate hostlists,
based on certain criteria.

%prep
%setup -q

%build
make

%install
make PREFIX=$RPM_BUILD_ROOT install
# prevent clash of similar-named docs
for i in perl-CAF-*/{ChangeLog,CHANGES,README,TODO}; do
  mv "$i" "$i".CAF
done
for i in  perl-LC-*/ChangeLog; do
  mv "$i" "$i".LC
done

%files
%defattr(-,root,root)
%{_bindir}/wassh
%{_libexecdir}/parallel
%{_libexecdir}/ssh-probe
%{_libexecdir}/wassh-decorate
%{_libexecdir}/wassh-files
%{perl_vendorlib}/LC/*.pm
%{perl_vendorlib}/CAF/*.pm

%doc /usr/share/man/man8/wassh.8.gz
%doc /usr/share/man/man8/parallel.8.gz
%doc /usr/share/man/man8/wassh-decorate.8.gz
%doc /usr/share/man/man8/wassh-files.8.gz
%config(noreplace) /etc/wassh/wassh.conf
%doc README
%doc perl-CAF-*/{ChangeLog,CHANGES,README,TODO}.CAF
%doc perl-LC-*/ChangeLog.LC

%files ssm-cern
%defattr(-,root,root)
%{perl_vendorlib}/wassh_ssm/*.pm
%doc /usr/share/man/man8/wassh*8pm.gz

%changelog
* Wed Dec 14 2022 Jan Iven <jan.iven@cern.ch> - 3.6.0
- in-source perl-CAF and perl-LC, 'parallel' in py3, CS9 compat, tests

* Mon Nov 30 2020 Jan Iven <jan.iven@cern.ch>
- rebuilt on C8: explicit dependency on perl-JSON

* Thu Aug 09 2018 Jan Iven <jan.iven@cern.ch> - 3.5.0
- puppetdb.pm: adapt to v4 endpoint

* Mon Jan 29 2018 Jan Iven <jan.iven@cern.ch> - 3.4.0
- IPv6 support (patch: Ulrich.Schwickerath@cern.ch)

* Mon Apr 24 2017 Jan Iven <jan.iven@cern.ch> - 3.3.5
- urlencode the query to Foreman (patch: Ignacio.Reguero@cern.ch)

* Mon Aug 22 2016 Jan Iven <jan.iven@cern.ch> - 3.3.4
- announce 'SSH-2.0' protocol when probing to remove warnings

* Mon Aug 15 2016 Jan Iven <jan.iven@cern.ch> - 3.3.3
- Foreman default URL should be single host to avoid GSSAPI errors

* Mon Jul 18 2016 Jan Iven <jan.iven@cern.ch> - 3.3.2
- PuppetDB URL should be single host to avoid GSSAPI errors

* Tue Apr 05 2016 Jan Iven <jan.iven@cern.ch> - 3.3.1
- update PuppetDB default entry point

* Tue Sep 15 2015 Jan Iven <jan.iven@cern.ch> - 3.3.0
- use puppetDB v4 API

* Tue Jun 30 2015  Jan Iven <jan.iven@cern.ch> - 3.2.0
- remove obsolete cdbsql SSM

* Fri Dec 12 2014  Jan Iven <jan.iven@cern.ch> - 3.1.0
- packaging: build wassh and wassh-ssm-cern together, move to GIT
- foreman.pm: support foreman-1.6 (patch by J.v.Eldik)
- add puppetdb.pm

* Fri Feb 07 2014 Jan Iven <jan.iven@cern.ch>
- wassh: allow multiple and per-user SSMs,
- parallel: use subprocess instead of popen2 (and fix a poll() bug introduced this way)

* Tue Jul 16 2013 Jan Iven <jan.iven@cern.ch>
- update to look at puppet/foreman

* Fri Jun 24 2011 Veronique Lefebure
- fix bugs reports in https://savannah.cern.ch/bugs/?57714

* Mon Jun 20 2011 Veronique Lefebure
- bug fix for perl

* Wed Nov 19 2008 Veronique Lefebure
- bug fix by Alex

* Mon Dec 03 2007 Rohitashva Sharma
- Support for stages, require wassh-ssm-cern 2.0.5

* Wed Jan 31 2007 German Cancio Melia
- Fixed missing carriage return in --list

* Thu Nov 09 2006 Rohitashva Sharma 
- changed output formatting

* Tue Nov 07 2006 German Cancio Melia
- [your comment here]

* Tue Nov 07 2006 German Cancio Melia
- arguments to wassh-decorate wrapped between single quotes (Sharma)

* Tue Oct 10 2006 German Cancio Melia
- fixes by Sharma

* Thu Sep 21 2006 German Cancio Melia
- Version 2 of wassh!

* Thu Sep 21 2006 German Cancio Melia
- Added 'ssm' option, now with default=yes, will load module if installed and otherwise skip silently

* Thu Sep 21 2006 German Cancio Melia
- Adapted to new SSM loading conventions

* Mon Sep 18 2006 Rohitashva Sharma <Rohitashva.Sharma@barc.gov.in>
- Initial Release
